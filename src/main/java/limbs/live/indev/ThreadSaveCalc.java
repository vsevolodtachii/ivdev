package limbs.live.indev;

import limbs.live.indev.data.Operator;
import limbs.live.indev.data.Result;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class ThreadSaveCalc extends Thread {
    private ConcurrentLinkedQueue<Operator> queueIn = new ConcurrentLinkedQueue<>();
    private Map<Long, Result> mapOut = new ConcurrentHashMap<>();
    private indev.test.job.calc.ICalc iCalc;
    private AtomicBoolean stop = new AtomicBoolean(false);

    private void calculate(Operator operator) {
        try {
            switch (operator.getOp()) {
                case '+':
                    mapOut.put(operator.getTaskId(), new Result(iCalc.Add(operator.getOp1(), operator.getOp2())));
                    break;
                case '*':
                    mapOut.put(operator.getTaskId(), new Result(iCalc.Multiply(operator.getOp1(), operator.getOp2())));
                    break;
                case '/':
                    mapOut.put(operator.getTaskId(), new Result(iCalc.Divide(operator.getOp1(), operator.getOp2())));
                    break;
                case '-':
                    mapOut.put(operator.getTaskId(), new Result(iCalc.Add(operator.getOp1(), -operator.getOp2())));
                    break;
                default:
                    mapOut.put(operator.getTaskId(), new Result("error wrong operator"));
                    break;
            }
        } catch (Exception ex) {
            mapOut.put(operator.getTaskId(), new Result("sum error: " + ex.getMessage()));
        }
    }


    @Override
    public void run() {
        iCalc = indev.test.job.calc.ThreadCalc.Instance();
        while (!stop.get()) {
            while (!queueIn.isEmpty()) {
                calculate(queueIn.poll());
            }
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                stop.set(true);
            }
        }

    }

    public void setStop() {
        this.stop.set(true);
    }

    public Result get(Long id) {
        return mapOut.get(id);
    }

    public Result blockGet(Long id) {
        Result temp;
        while (true) {
            temp = get(id);
            if (temp != null) {
                return temp;
            }
        }
    }

    public void add(Operator o) {
        queueIn.add(o);
    }
}
