package limbs.live.indev.data;

public class Result {
    private String result;
    private Double value;

    @Override
    public boolean equals(Object obj) {
        return this.result.equals(((Result) obj).result) && this.value.equals(((Result) obj).value);
    }

    @Override
    public int hashCode() {
        return result.hashCode() + value.hashCode();
    }

    public Result(Double value) {
        this.value = value;
        this.result = "ok";
    }

    public Result(String result) {
        this.value = null;
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public Double getValue() {
        return value;
    }
}
