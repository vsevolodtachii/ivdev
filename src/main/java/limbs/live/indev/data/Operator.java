package limbs.live.indev.data;


public class Operator {
    private Double op1 = 0D;
    private Double op2 = 0D;
    private Character op = '+';

    private final long taskId;

    public Operator(){
        this.taskId = Thread.currentThread().getId();
    }

    public Double getOp2() {
        return op2;
    }

    public void setOp2(Double op2) {
        this.op2 = op2;
    }

    public Character getOp() {
        return op;
    }

    public void setOp(Character op) {
        this.op = op;
    }

    public Double getOp1() {
        return op1;
    }

    public void setOp1(Double op1) {
        this.op1 = op1;
    }

    public long getTaskId() {
        return taskId;
    }
}
