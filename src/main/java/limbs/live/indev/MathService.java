package limbs.live.indev;

import limbs.live.indev.data.Operator;
import limbs.live.indev.data.Result;
import org.springframework.stereotype.Service;

@Service
public class MathService {

    private ThreadSaveCalc calc = new ThreadSaveCalc();

    public MathService() {
        calc.start();
    }

    public Result process(Operator operator) {
        calc.add(operator);
        return calc.blockGet(operator.getTaskId());
    }
}
