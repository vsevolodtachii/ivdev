package limbs.live.indev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndevApplication {

	public static void main(String[] args) {
		SpringApplication.run(IndevApplication.class, args);
	}

}
