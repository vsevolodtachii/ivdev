package limbs.live.indev.rest;


import limbs.live.indev.MathService;
import limbs.live.indev.data.Operator;
import limbs.live.indev.data.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/count")
public class MainController {

    private final MathService mathService;

    @Autowired
    public MainController(MathService mathService) {
        this.mathService = mathService;
    }

    @PostMapping
    @ResponseBody
    public Result getTestData(@RequestBody Operator operator) {
        return mathService.process(operator);
    }
}
